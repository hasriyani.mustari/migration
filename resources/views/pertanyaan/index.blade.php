@extends('partial.master')

@section('content')
    <div class="mt-3 ml-3">
        <div class="card">
            <div class="card-header">
              <h3 class="card-title">Table Pertanyaan</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                @if (session('success'))
                    <div class="alert alert-success">
                        {{session('success') }}
                    </div>
                @endif
                <a class="btn btn-primary mb-2" href="{{route('pertanyaan.create') }}">Buat Pertanyaan</a>
              <table class="table table-bordered">
                <thead>                  
                  <tr>
                    <th style="width: 10px">#</th>
                    <th>Judul</th>
                    <th>Isi</th>
                    <th style="width: 40px">Aksi</th>
                  </tr>
                </thead>
                <tbody>
                  @forelse ($pertanyaan as $key => $post)
                      <tr>
                          <td>{{$key+1}} </td>
                          <td>{{$post-> judul}} </td>
                          <td>{{$post-> isi}} </td>
                          <td style="display:flex">
                              <a href="{{route('pertanyaan.show', ['pertanyaan' => $post->id])}} "  class="btn btn-info btn-sm">tampilkan</a>
                              <a href="{{route('pertanyaan.edit', ['pertanyaan' => $post->id])}} "  class="btn btn-default btn-sm">edit</a>
                              <form action="{{route('pertanyaan.destroy', ['pertanyaan' => $post->id])}} " method="POST">
                                @csrf
                                @method('DELETE')
                                <input type="submit" value="delete" class="btn btn-danger btn-sm">
                            </form>
                          </td>
                      </tr>
                    @empty
                        <tr>
                            <td colspan="4" align="center"> Tidak Ada Pertanyaan</td>
                        </tr>
                  @endforelse
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
    </div>
@endsection