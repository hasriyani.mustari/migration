<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Question;

class PertanyaanController extends Controller
{
    public function create(){
        return view('pertanyaan.create');
    }

    public function store(Request $request){
        //dd($request->all());
        $request->validate([
            'judul' => 'required|unique:pertanyaan',
            'isi' => 'required'
        ]);
        // $query = DB::table('pertanyaan')->insert([
        //     "judul"=> $request["judul"],
        //     "isi"=> $request["isi"]
        // ]);

        //--Eloquent--
        // $question = new Question;
        // $question->judul = $request['judul'];
        // $question->isi = $request['isi'];
        // $question->save(); //insert into question (judul, isi) values

        //-mass asignment--
        $question = Question::create([
            'judul' => $request['judul'],
            'isi' => $request['isi']
        ]);
        return redirect('/pertanyaan')->with('success', 'Pertanyaan Berhasil Disimpan!');
    }

    public function index(){
        //$pertanyaan = DB::table('pertanyaan')->get(); //select * from pertanyaan
        //dd($pertanyaan);

        $pertanyaan = Question::all();
        //dd($pertanyaan);
        return view('pertanyaan.index', compact('pertanyaan'));
    }

    public function show($pertanyaan_id){
        // $post = DB::table('pertanyaan')->where('id', $pertanyaan_id)->first();
        $post = Question::find($pertanyaan_id);
        //dd($post);
        return view('pertanyaan.show', compact('post'));
    }

    public function edit($pertanyaan_id){
        // $post = DB::table('pertanyaan')->where('id', $pertanyaan_id)->first();
        $post = Question::find($pertanyaan_id);
        return view('pertanyaan.edit', compact('post'));
    }

    public function update($pertanyaan_id, Request $request){
        $request->validate([
            'judul' => 'required',
            'isi' => 'required'
        ]);
        // $query = DB::table('pertanyaan')
        //             ->where ('id', $pertanyaan_id)
        //             ->update([
        //                 "judul"=> $request["judul"],
        //                 "isi"=> $request["isi"]
        //             ]);
        $update = Question::where('id', $pertanyaan_id)->update([
            "judul"=> $request["judul"],
            "isi"=> $request["isi"]
        ]);
        return redirect('/pertanyaan')->with('success','Berhasil Update Pertanyaan!');
    }

    public function destroy($pertanyaan_id){
        // $query = DB::table('pertanyaan')->where('id', $pertanyaan_id)->delete();
        Question::destroy($pertanyaan_id);
        return redirect('/pertanyaan')->with('success','Pertanyaan berhasil Dihapus!');
    }
}
