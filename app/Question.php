<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $table = 'pertanyaan';

    protected $fillable = ['judul', 'isi'];//mass assignment
    //protected $quarded = ['value']; //digunakan ketika table memiliki banyak column, value diisikan nama column yang tidak ingin diisi
}
